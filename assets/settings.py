from pydantic import BaseModel

class Settings(BaseModel):
    min_dicom_len: int=5
    max_thickness: float=10
    max_spacing: float=2
    thickness_consistency_check_tolerance: float=0.5