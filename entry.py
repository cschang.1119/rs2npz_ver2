from core.rs2npz import RStoNPZ
import pandas as pd
import pydicom
from glob import glob
from utils.dcm_checker import DicomChecker
from utils.rs_to_volume import load_sorted_image_series
from assets.settings import Settings
import os
from tqdm import tqdm


def main():
    st = Settings()
    dst_path = "/output_dir"
    rstonpz = RStoNPZ(dst_path)
    dcm_checker = DicomChecker(st)
    
    df = pd.read_csv("/dir_to_csv")

    for i, row in tqdm(df.iterrows()):
        if i > 26:
            ct_folder = row['New_dicom_folder']
            ct_dcms = glob(os.path.join(ct_folder, "*dcm"))
            ds_list = load_sorted_image_series(ct_dcms)

            rs_dcm = row['RS_path']
            rs_ds = pydicom.read_file(rs_dcm)
            in_list = [ds for ds in ds_list if ds.SeriesInstanceUID == row['SeriesInstanceUID']]
            check, modality = dcm_checker(in_list)
            if check:
                rstonpz(in_list, rs_ds, ct_dcms, rs_dcm)
            else:
                f = open(f"{dst_path}/error_cases.txt", "a")
                print(f"{row['SeriesInstanceUID']} check failed.", file=f)
                f.close()


if __name__ == "__main__":
    main()
