import pydicom
import numpy as np
from pydicom import Dataset
from typing import List

class DicomChecker():
    def __init__(self, st):
        self.check: bool
        self.st = st
        self.ds_list: List[Dataset]

    def __call__(self, ds_list: List[Dataset]):
        self.ds_list = ds_list
        self.check = True
        
        modality = self.check_modality()
        checkers = self.set_checker(modality)
            
        for checker in checkers:
            if self.check:
                try:
                    checker()
                except Exception as e:
                    print(e)
            else:
                break
        return self.check, modality

    def set_checker(self, modality: str):
        base_checkers = [
            self.check_ds_same_series,
            self.check_dcm_len,
            self.check_axial,
            self.check_image_original,
            self.check_max_thickness,
            self.check_max_spacing,
            self.check_thickness_consistency
        ]

        mr_checkers = [
            self.check_mr_2D,
            self.check_mr_scan_sequence,
        ]

        if modality == "CT":
            checkers = base_checkers
        elif modality == "MR":
            checkers = base_checkers + mr_checkers
        return checkers

    def check_ds_same_series(self):
        series_list = [ds.SeriesInstanceUID for ds in self.ds_list]
        uniqued_series = list(set(series_list))
        if len(uniqued_series) > 1:
            self.check = False
            raise ValueError("Dicom files are more than one series.")

    def check_dcm_len(self):
        if len(self.ds_list) < self.st.min_dicom_len:
            self.check = False
            raise ValueError("length of dicom is less than 5.")

    def check_modality(self):
        modalities = []
        for ds in self.ds_list:
            if ds.Modality == "CT":
                modalities.append(ds.Modality)
                pass
            elif ds.Modality == "MR":
                modalities.append(ds.Modality)
                pass
            else:
                self.check = False
                raise KeyError(f"SOP {ds.SOPInstanceUID} Wrong modality {ds.Modality}.")
        return list(set(modalities))[0]

    def check_max_thickness(self):
        image_position_list = [float(ds.ImagePositionPatient[-1]) for ds in self.ds_list]
        image_position_list.sort()
        for thickness in np.diff(image_position_list):
            if thickness >= self.st.max_thickness:
                self.check = False
                raise ValueError(f"Thickness > {self.st.max_thickness}.")
            

    def check_max_spacing(self):
        for ds in self.ds_list:
            if ds.PixelSpacing[1] >= self.st.max_spacing:
                self.check = False
                raise ValueError(f"Spacing > {self.st.max_spacing}.")
            

    def check_thickness_consistency(self):
        image_position_list = [float(ds.ImagePositionPatient[-1]) for ds in self.ds_list]
        image_position_list.sort()
        slice_thickness_list = np.diff(image_position_list)
        for thick_diff in np.diff(slice_thickness_list):
            if abs(thick_diff) <= self.st.thickness_consistency_check_tolerance:
                pass
            else:
                self.check = False
                raise ValueError("Thickness is inconsistency.")

    def check_axial(self):
        for ds in self.ds_list:
            if "AXIAL" in ds.ImageType:
                pass
            else:
                self.check = False
                raise KeyError(f"{ds.SOPInstanceUID} Image not Axial.")

    def check_image_original(self):
        for ds in self.ds_list:
            if "ORIGINAL" in ds.ImageType:
                pass
            else:
                self.check = False
                raise KeyError(f"SOP {ds.SOPInstanceUID} Image not ORIGINAL.")

    def check_mr_2D(self):
        for ds in self.ds_list:
            if ds.MRAcquisitionType == "2D":
                pass
            else:
                self.check = False
                raise KeyError(f"SOP {ds.SOPInstanceUID} not 2D image.")

    def check_mr_scan_sequence(self):
        for ds in self.ds_list:
            if "SE" in ds.ScanningSequence:
                pass
            elif "IR" in ds.ScanningSequence:
                pass
            elif "GR" in ds.ScanningSequence:
                pass
            else:
                self.check = False
                raise KeyError(f"SOP {ds.SOPInstanceUID} wrong ScanningSequence.")
