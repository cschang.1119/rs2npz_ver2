import cv2
import numpy as np

from typing import Dict, List, cast
from pydicom import Dataset
from scipy.ndimage import rotate


class RStoMask():
    """
    Class: RStoMask

    This class represents a conversion tool for converting CT DICOM data to masks. It provides methods for sorting CT data, transforming CT slices to 2D images, and adding RS DICOM data to generate masks.

    Methods:
    - __init__(self, ct_dict, organs): Initializes the RStoMask object with CT data and organ information.
    - sort_ct(self, ct_list, frameref): Sorts the CT data based on the frame of reference and returns the sorted CT data as a numpy array.
    - transform_ct_data(ct): Transforms a single CT DICOM slice to a 2D image.
    - calc_rotation_angle(current_orientation, desired_orientation): Calculates the rotation angle between two direction cosines.
    - add_rs_dicom(self, dcm, ct_images): Adds RS DICOM data to generate masks and returns the masks as a dictionary.

    """
    def __init__(self, ct_dict, organs):
        self._data: Dict[str, List[Dataset]] = ct_dict
        self.organs: Dict[int, str] = organs

    def sort_ct(self, ct_list, frameref):
        """
        Check if the patient position are identical, and sort the z space by the ImagePositionPatient tag.

        Args:
            ct_list: List[ds]
            frameref: str

        Retruns:
            sorted ct data structure in np array.
        """

        filtered_data = []
        for d in ct_list:
            if d.FrameOfReferenceUID == frameref:
                filtered_data.append(d)
        try:
            reverse = [(d.PatientPosition[0:2] == "FF") for d in filtered_data]
        except AttributeError as e:
            raise AttributeError(f"{e} PatientPosition was not found.")

        # Check that position is the same in all data
        if all(reverse) != any(reverse):
            raise ValueError("Inconsistent PatientPosition")
        # Sort the filtered_data
        try:
            filtered_data.sort(reverse=all(reverse),
                               key=lambda d: d.ImagePositionPatient[2])
        except  AttributeError as e:
            raise AttributeError(f"{e} ImagePositionPatient was not found.")

        self._data[frameref] = filtered_data
        # Build the np.array
        cts_list = [self.transform_ct_data(d) for d in filtered_data]
        cts = np.array(cts_list)
        return cts

    @staticmethod
    def transform_ct_data(ct: Dataset) -> np.ndarray:
        """Transform a single CT DICOM (Slice) to a 2D Image

        Args:
            ct: The pydicom dataset of a single CT slice

        Returns:
            2D Array of CT values in HU, flipped in the proper direction.
        """
        common_orientation = [1, 0, 0, 0, 1, 0]
        ct_data = ct.pixel_array * ct.RescaleSlope + ct.RescaleIntercept
        current_orientation = ct.ImageOrientationPatient
        rotation_angle = RStoMask.calc_rotation_angle(current_orientation, 
                                                      common_orientation)
        ct_data = rotate(ct_data, rotation_angle, reshape=False)

        return ct_data

    @staticmethod
    def calc_rotation_angle(current_orientation, desired_orientation):
        """
        Calculate the rotation angle between two orientation vectors.
        
        Args:
            current_orientation (list): The current orientation vector specified as direction cosines.
            desired_orientation (list): The desired orientation vector specified as direction cosines.
            
        Returns:
            float: The rotation angle in degrees.
        """
        # Convert input lists to numpy arrays
        current_direction_cosines = np.array(current_orientation[:3])
        desired_direction_cosines = np.array(desired_orientation[:3])
        
        # Calculate the dot product between the two direction cosines
        dot_product = np.dot(current_direction_cosines, desired_direction_cosines)
        
        # Calculate the angle between the two direction cosines using the dot product
        angle = np.arccos(dot_product)
        
        # Convert the angle from radians to degrees
        angle_degrees = np.degrees(angle)
        
        return angle_degrees

    def add_rs_dicom(self, ds, ct_images) -> Dict[str, np.ndarray]:
        """
        Extract the RT contour data to mask array.

        Args:
            ds: DataStructure
            ct_images: np.ndarray
        
        Returns:
            np.ndarray: mask array [c, z, x, y]
        """
        # Read everything from ROIs
        common_orientation = [1, 0, 0, 0, 1, 0]
        mask = {}
        FrameOfReferenceUID = []
        for s in ds.StructureSetROISequence:
            FrameOfReferenceUID.append(s.ReferencedFrameOfReferenceUID)
        for frameref in np.unique(FrameOfReferenceUID):
            # Build a zeros mask with the size of CT and len of organs
            arr = cast(np.ndarray, ct_images)
            mask[frameref] = np.zeros(list(arr.shape)
                                      + [len(self.organs)],
                                      dtype=bool)
            # Get organ numbers from StructureSetROISequence
            INV_ORGANS = {v: k for k, v in self.organs.items()}
            organ_map = {
                s.ROINumber: INV_ORGANS[s.ROIName]
                for s in ds.StructureSetROISequence
                if s.ReferencedFrameOfReferenceUID == frameref
                and s.ROIName in INV_ORGANS.keys()
            }
            slice_map = {}
            try:
                for i, d in enumerate(self._data[frameref]):
                    slice_map.update({d.SliceLocation: i})
            except Exception:
                for i, d in enumerate(self._data[frameref]):
                    slice_map.update({d.ImagePositionPatient[2]: i})
            
            # Read contours (Loop for different categories)
            for conseq in ds.ROIContourSequence:
                try:
                    mask_num = organ_map[conseq.ReferencedROINumber]
                except KeyError:
                    continue  # This will skip the ROIs that are not in ORGANS
                if conseq.get((0x3006, 0x0040), None) is None:
                    continue

                # Loop for different slices
                for c in conseq.ContourSequence:
                    if c.ContourGeometricType != "CLOSED_PLANAR":
                        continue
                    sop_uids = []
                    dcm_lst = []
                    for r in c.ContourImageSequence:
                        sop_uids.append(r.ReferencedSOPInstanceUID)
                    for d in self._data[frameref]:
                        if d.SOPInstanceUID in sop_uids:
                            dcm_lst.append(d)
                    # Check only have one slice,
                    # since we only support 2D contours
                    assert len(dcm_lst) == 1
                    slice_dcm = dcm_lst[0]
                    # Check that contour data are in x,y,slice
                    assert len(c.ContourData) == c.NumberOfContourPoints * 3
                    one_contour = np.array(c.ContourData).reshape(-1, 3)
                    # Check that the last dimension is the same and
                    # it's the same as slice.SliceLocation
                    # assert np.unique(one_contour[:, 2])[0]
                    #                  == slice_dcm.SliceLocation
                    x_spacing, y_spacing = (float(slice_dcm.PixelSpacing[0]),
                                            float(slice_dcm.PixelSpacing[1]))
                    z_thickness = slice_dcm.SliceThickness
                    # check here
                    x_origin, y_origin, z_origin = \
                        slice_dcm.ImagePositionPatient
                    one_contour[:, 0] = ((one_contour[:, 0]-x_origin)
                                         / x_spacing)
                    one_contour[:, 1] = ((one_contour[:, 1]-y_origin)
                                         / y_spacing)
                    one_contour[:, 2] = ((one_contour[:, 2]-z_origin)
                                         / z_thickness)
                    if slice_dcm.PatientPosition[2] == "P":
                        Ax, Ay, Az, Bx, By, Bz = \
                             slice_dcm.ImageOrientationPatient
                        Cx = Ax * Bx
                        Cy = Ay * By
                        Cz = Az * Bz
                        matrix = np.array([[Ax, Bx, Cx],
                                           [Ay, By, Cy],
                                           [Az, Bz, Cz]])
                        one_contour = np.dot(one_contour, matrix)
                    slice_mask = np.zeros(slice_dcm.pixel_array.shape,
                                          dtype=np.uint8)
                    current_orientation = slice_dcm.ImageOrientationPatient
                    rotation_angle = RStoMask.calc_rotation_angle(
                                        current_orientation,
                                        common_orientation)
                    
                    # Draw contours
                    cv2.fillPoly(
                        slice_mask,
                        pts=np.array([one_contour[:, :2]], dtype=np.int32),
                        color=(255, 255, 255),
                    )
                    slice_mask = rotate(slice_mask,
                                        rotation_angle,
                                        reshape=False)
                    # XOR is associative
                    try:
                        slice_num = slice_map[slice_dcm.SliceLocation]
                    except Exception:
                        slice_num = slice_map[
                            slice_dcm.ImagePositionPatient[2]
                            ]
                    mask[frameref][slice_num, :, :, mask_num] = np.logical_xor(
                        mask[frameref][slice_num, :, :, mask_num],
                        slice_mask.astype(bool),
                    )
        return mask
