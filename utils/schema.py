from typing import Any, Dict, List, Optional
from pydantic import BaseModel


class DicomTagsDict(BaseModel):
    Rows: int
    Columns: int
    Modality: str
    PatientID: str
    StudyInstanceUID: str
    SeriesInstanceUID: str
    SliceThickness: float
    PixelSpacing: List[float]
    RescaleSlope: Any
    RescaleIntercept: Any
    ImagePositionPatient: list
    PatientPosition: str
    ImageOrientationPatient: List[int]
    Manufacturer: Optional[str] = "NA"
    InstitutionName: Optional[str] = "NA"
    PatientSex: Optional[str] = "NA"
    PatientAge: Optional[str] = "NA"
    SeriesDescription: str


class MaskInfoDict(BaseModel):
    Modality: str
    LabelDescription: str
    RS_SOPInstanceUID: str
    Label_version: str
    bbox: Dict[str, int]


class AnnoInfoDict(BaseModel):
    ROINames: List[str]
    masks_info: Dict[str, MaskInfoDict]


class NPZSchema(BaseModel):
    format_version: str = "v1.0"
    slice_num: int
    Dicom_tags: DicomTagsDict
    Annotation_info: AnnoInfoDict
    pixel_array: Any
