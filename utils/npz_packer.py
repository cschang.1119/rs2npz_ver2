import numpy as np

from utils.__init__ import __version__
from utils.schema import DicomTagsDict, AnnoInfoDict, NPZSchema, MaskInfoDict


class NPZGenerator():
    """
    NPZGenerator will generate the dictionary which is fit the defined format.

    """
    def __init__(self, organs):
        """
        Args:
            organs: Dict[str]
        """
        self.organs = organs

    def pack_npz(self, ds, rs, image, mask_dict):
        """
        Args:
            ds: CT data structure
            rs: RS data structure
            image: np.ndarray()
            mask_dict: Dict[np.array]

        Returns:
            npz_dict: Dict[]
        """
        dicom_tags_dict = DicomTagsDict(
            Rows=ds.Rows if "Rows" in ds else "NA",
            Columns=ds.Columns if "Columns" in ds else "NA",
            Modality=ds.Modality if "Modality" in ds else "NA",
            PatientID=ds.PatientID if "PatientID" in ds else "NA",
            StudyInstanceUID=ds.StudyInstanceUID if "StudyInstanceUID" in ds else "NA",
            SeriesInstanceUID=ds.SeriesInstanceUID if "SeriesInstanceUID" in ds else "NA",
            SliceThickness=ds.SliceThickness if "SliceThickness" in ds else "NA",
            PixelSpacing=list(ds.PixelSpacing) if "PixelSpacing" in ds else "NA",
            RescaleSlope=ds.RescaleSlope if "RescaleSlope" in ds else "NA",
            RescaleIntercept=ds.RescaleIntercept if "RescaleIntercept" in ds else "NA",
            ImagePositionPatient=list(ds.ImagePositionPatient) if "ImagePositionPatient" in ds else "NA",
            PatientPosition=ds.PatientPosition if "PatientPosition" in ds else "NA",
            ImageOrientationPatient=list(ds.ImageOrientationPatient) if "ImageOrientationPatient" in ds else "NA",
            InstitutionName=ds.InstitutionName if "InstitutionName" in ds else "NA",
            Manufacturer=ds.Manufacturer if "Manufacturer" in ds else "NA",
            PatientSex=ds.PatientSex if "PatientSex" in ds else "NA",
            PatientAge=ds.PatientAge if "PatientAge" in ds else "NA",
            SeriesDescription=ds.SeriesDescription if "SeriesDescription" in ds else "NA"
        )

        masks_dict = {}
        for idx, OAR in self.organs.items():
            z, x, y = np.where(mask_dict[OAR])

            if len(z) != 0:
                bboxs = {
                    "zmin": min(z),
                    "zmax": max(z),
                    "xmin": min(x),
                    "xmax": max(x),
                    "ymin": min(y),
                    "ymax": max(y)
                }
            else:
                bboxs = {
                    "zmin": 0,
                    "zmax": 0,
                    "xmin": 0,
                    "xmax": 0,
                    "ymin": 0,
                    "ymax": 0
                }

            masks_dict.update({OAR: MaskInfoDict(
                Modality=rs.Modality,
                LabelDescription=idx,
                RS_SOPInstanceUID=rs.SOPInstanceUID,
                Label_version="test_01",
                bbox=bboxs
            )})

        annotation_info_dict = AnnoInfoDict(
            ROINames=list(self.organs.values()),
            masks_info=masks_dict
        )

        npz_dict = NPZSchema(
            format_version=__version__,
            slice_num=int(image.shape[0]),
            Dicom_tags=dicom_tags_dict.dict(),
            Annotation_info=annotation_info_dict.dict(),
            pixel_array=image,
        )
        return npz_dict.dict()
