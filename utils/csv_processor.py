import os

from typing import Type


class CsvProcessor():
    """
    CsvProcessor

    Class for handling CSV file operations and packing DICOM information.
    """
    def __init__(self, output_dir):
        # Initialize CsvProcessor with an output directory
        self.output_dir = output_dir

        # Define DICOM tags with their respective types
        self.dcm_tags = {
            "patient_id": Type[str],
            "study_id": Type[str],
            "series_id": Type[str],
            "accnum": Type[str],
            "ct_folder": Type[str],
            "rs_location": Type[str],
            "npz_folder": Type[str],
            "age": Type[str],
            "gender": Type[str],
            "manufacturer": Type[str]
        }

    def create_csv(self):
        # Create a CSV file if it does not exist, otherwise print a message
        os.makedirs(self.output_dir, exist_ok=True)
        if os.path.isfile(os.path.join(self.output_dir, "npz_info.csv")):
            print("csv existed.")
        else:
            with open(os.path.join(self.output_dir, "npz_info.csv"), "w") as f:
                # Write headers to the CSV file
                f.write("patient_id,study_id,series_id,accession_number,ct_folder,rs_location,npz_folder,age,gender,manufacturer\n")

    def pack_dcm_info(self, ds, ct_paths, rs_path, output_directory):
        """
        Pack DICOM information into the class attributes.

        Args:
            ds: CT data structure (DICOM object)
            ct_paths: List of strings representing CT file paths
            rs_path: String representing the path to the RS file
            output_directory: String representing the output directory
        """
        # Pack DICOM information into respective attributes
        self.dcm_tags['patient_id'] = ds.PatientID
        self.dcm_tags['study_id'] = ds.StudyInstanceUID
        self.dcm_tags['series_id'] = ds.SeriesInstanceUID
        self.dcm_tags['ct_folder'] = os.path.dirname(ct_paths[0])
        self.dcm_tags['rs_location'] = rs_path
        self.dcm_tags['npz_folder'] = output_directory
        if "AccessionNumber" in ds:
            self.dcm_tags['accnum'] = ds.AccessionNumber
        else:
            self.dcm_tags['accnum'] = "na"
        if "Manufacturer" in ds:
            self.dcm_tags['manufacturer'] = ds.Manufacturer
        else:
            self.dcm_tags['manufacturer'] = "na"
        if "PatientAge" in ds:
            self.dcm_tags['age'] = ds.PatientAge
        else:
            self.dcm_tags['age'] = "na"
        if 'PatientSex' in ds:
            self.dcm_tags['gender'] = ds.PatientSex
        else:
            self.dcm_tags['gender'] = 'na'

    def write_csv(self):
        # Write DICOM information to the CSV file
        with open(os.path.join(self.output_dir, "npz_info.csv"), "a") as f:
            # Write DICOM values as a new line in the CSV file
            f.write(f"{self.dcm_tags['patient_id']},{self.dcm_tags['study_id']},{self.dcm_tags['series_id']},{self.dcm_tags['accnum']},{self.dcm_tags['ct_folder']},{self.dcm_tags['rs_location']},{self.dcm_tags['npz_folder']},{self.dcm_tags['age']},{self.dcm_tags['gender']},{self.dcm_tags['manufacturer']}\n")
