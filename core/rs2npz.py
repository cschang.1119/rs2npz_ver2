import os
import numpy as np

from typing import Dict, List
from pydicom import Dataset, dcmread
from utils.csv_processor import CsvProcessor
from utils.npz_packer import NPZGenerator
from utils.contour2mask import RStoMask
from utils.rs_to_volume import get_img_volume, get_roi_mask_dict, flip_volume_by_position, flip_volume_by_position


class RStoNPZ():
    """
    class: RStoNPZ

    When initializing RStoNPZ, the input will be a directory path where the
    output will be stored.

    The input for RStoNPZ consists of a series of CT DICOM paths along with
    their corresponding RT Structure (RS) path.

    the definition please check:
    https://www.notion.so/RT_CTDICOM_RTSTRUCT_-0a0bad33abb449d49de41b6f3dcc04fe?pvs=4#e1e8f1bdcd9b48d88dc69f63f2ef6cfa
    """

    def __init__(self, dst_dir):
        """
        dst_dir: str = absolute path.
        """
        self.dst_dir = dst_dir
        self.organs: Dict[int, str] = {}
        self.csv_processor = CsvProcessor(self.dst_dir)
        self.csv_processor.create_csv()

    def __call__(self, ds_list, rs_ds, ct_paths, rs_path):
        """
        rs_path: str
        """

        # Extract the ROI names from rs dicom.
        self.organs = self.roi_extractor(rs_ds)

        npz_packer = NPZGenerator(self.organs)
#         ct_processor = RStoMask(ct_dict, self.organs)

        # Obtain ct images and masks from referenced series
        ct_images = get_img_volume(ds_list)
        mask_dict = get_roi_mask_dict(rs_ds, ds_list)
        # 要根據pos翻轉下三行註解取消
        img_volume = flip_volume_by_position(ct_images, ds_list)   # flip image_volume
        for mask_key in mask_dict.keys():                            # flip mask_volume
            mask_dict[mask_key] = flip_volume_by_position(mask_dict[mask_key], ds_list)
#         ct_images = ct_processor.sort_ct(ds_list, frameref)
#         npz_masks = ct_processor.add_rs_dicom(rs_ds, ct_images)[frameref]
#         npz_masks = np.transpose(npz_masks, [3, 0, 1, 2])
        dcm = ds_list[0]
        # Pack dicom tags, ct images, and masks
        # to the defined format.
        npz_dict = npz_packer.pack_npz(dcm, rs_ds, img_volume, mask_dict)
        
        for idx, OAR in self.organs.items():
            npz_dict.update({OAR: mask_dict[OAR]})
#         for idx, OAR in self.organs.items():
#             npz_dict.update({OAR: npz_masks[idx]})

        # Check if the file idx already existed in target directory.
        folder_idx = self.check_folder_index()
        output_directory = os.path.join(
            self.dst_dir, str(folder_idx))
        self.write_npz(npz_dict, output_directory)

        self.csv_processor.pack_dcm_info(
            dcm, ct_paths, rs_path, output_directory
            )
        self.csv_processor.write_csv()

    @staticmethod
    def write_npz(npz_dict, output_directory):
        """
        Args:
            npz_dict: Dict[]
            output_directory: str
        """
        os.makedirs(output_directory, exist_ok=False)
        np.savez_compressed(
            os.path.join(output_directory, 'data.npz'), **npz_dict
            )

    @staticmethod
    def roi_extractor(rs: Dataset) -> Dict[int, str]:
        """
        Extract all ROI names from RS dicom.

        Args:
            rs: Dataset

        Returns:
            ori_dict: Dict
        """
        ori_names = [roi_sequence.ROIName for roi_sequence in rs[0x30060020]]
        ori_dict = {i: ROI for i, ROI in enumerate(sorted(ori_names))}
        return ori_dict

    @staticmethod
    def load_ct_dicom_to_list(ct_paths: List[str]) -> List[Dataset]:
        """
        Load CT dicom into a list.

        Args:
            ct_paths: List[str]

        Returns:
            ct_list: List[Dataset]
        
        """
        ct_list = []
        for ct_path in ct_paths:
            ds = dcmread(ct_path)
            if ds.Modality == "CT":
                ct_list.append(ds)
        return ct_list

    @staticmethod
    def load_rs_dicom(rs_path: str) -> Dataset:
        """
        Load RS dicom and UID.

        Args:
            rs_path: str

        Returns:
            rs_ds: Dataset
            FrameOfReferenceUID: str
        
        """
        rs_ds = dcmread(rs_path)
        return rs_ds, rs_ds.FrameOfReferenceUID

    def check_folder_index(self):
        """
        Check the folder index from destination folder, avoid to overwrite existed files.

        Returns:
            index: int
        """
        max_idx = 0
        for item in os.listdir(self.dst_dir):
            if item.isdigit() and int(item) > max_idx:
                max_idx = int(item)
        return max_idx + 1

    @staticmethod
    def load_npz(src_path: str) -> Dict:
        """
        A tool for load npz data to a dict format.

        Args:
            src_path: str = "npz file directory"
        Returns:
            data_dict: Dict 
        """
        data_dict = {}
        data = np.load(src_path, allow_pickle=True)
        info_keys = ["format_version",
                     "slice_num",
                     "CT_dicom_tags",
                     "Annotation_info"]

        for key in list(data.keys()):
            if key in info_keys:
                data_dict.update({key: data[key].item()})
            else:
                data_dict.update({key: data[key]})
        return data_dict
